(function($) {
  $(document).ready(function() {

    var sessionData = [];

    // Process the JSON file
    (function processJSON() {
      $.getJSON( '/data/sessions.json', function(json) {
          processSessionData(json)
      }).fail(function(jqxhr, textStatus, error){
        var err = textStatus + ", " + error;
        console.error( "Request Failed: " + err );
      })
    })();

    // Simple function to return data from JSON in an array format based on track query or generic
    var returnData = function(data, track) {
      var jsonArr = []
      $.each(data.Items, function(key, val) {
        var q = val.Track.Title.toLowerCase();

        if (track != '' && q == track) jsonArr.push(val)
        else jsonArr.push(val)
      })
      return jsonArr
    }

    // Capture session data generically here and run tasks
    function processSessionData(json) {
      sessionData = returnData(json)

      // Functions

      // Change views
      function changeView(track, article) {
        // Clear views
        $('.session-aside, .session-view').html('')
        // Set the new view
        setView(sessionData, track, article)
      }

      $(document).on('click', '.js-change-view', function(e) {
        // get track and article here
        var track = $(this).attr('href').replace(/\?(.*)/,'').replace(/\#/, ''),
            article = $(this).attr('href').split('?article-id=')[1]
        changeView(track, article)
      })

      // Tasks

      setNav(sessionData)
    }

    // Set site navigation
    function setNav(data) {
      var nav = [], navDom = []
      var session = data

      // Set unique tracks to nav
      $.each(session, function(key, val) {
        if($.inArray(val.Track.Title, nav) === -1) nav.push(val.Track.Title);
      })

      // Create dom elements
      $.each(nav, function(k, trackTitle) {
        var track = trackTitle.toLowerCase();
        navDom.push(
          $('<li class="session-nav__item" data-track='+track+'><a class="session-nav__link js-change-view" href="#'+track+'">'+trackTitle+'</a></li>')
        )
      })

      // Append nav to dom
      $('.session-nav').append(navDom)

      // Set first view to active either by hash or by first track
      var defaultView = window.location.hash.substring(1) || nav[0].toLowerCase(),
          defaultArticle = getQueryVariable('article-id') || getFirstArticle(session, defaultView)
          // console.log('query', getQueryVariable('article-id'))
          // console.log('defaultView',defaultView)
          // console.log('defaultArticle',defaultArticle)
      setView(session, defaultView, defaultArticle)
    }

    //Get the first article
    var getFirstArticle = function(data, track) {
      var articles = []
      $.each(data, function(k,v) {
        if(v.Track.Title.toLowerCase() === track) articles.push(v.Id)
      })
      return articles[0]
    }

    // Set the views for all of the blog posts
    function setView(data, track, article) {
      // Set window location
      // window.location.hash = track;

      var session = data,
          activeTrack = track,
          activeArticle = article || getFirstArticle(session, activeTrack)
      var viewsDom = [], asideDom = [];

      $.each(session, function(key, val) {

        var id = val.Id,
            title = val.Title,
            desc = val.Description,
            speakers = val.Speakers ? val.Speakers[0] : '',
            speaker = val.Speakers ? speakers.FirstName + ' ' + speakers.LastName : '',
            speakerTitle = val.Speakers && speakers.Title ? ', ' + speakers.Title : '',
            bio = val.Speakers ? speakers.Biography : '',
            track = val.Track.Title,
            trackId = track.toLowerCase()


        // Define aside
        if ( activeTrack === trackId ) {
          // Create sidebar items
          var asideDomObj = " \
                            <li class='list-group-item session-aside__item' data-track='"+trackId+"' data-article='"+id+"'>  \
                            <a href='?article-id="+id+"#"+trackId+"' class='session-aside__link js-change-view'><h5 class='session-aside__title'>"+title+"</h5></a> \
                            <span class='session-aside__speaker'>"+speaker+speakerTitle+"</span> \
                            </li> \
                            ";
          asideDom.push(
            $(asideDomObj)
          )
        }

        // Define article
        if ( activeArticle == id ) {
          // Create article
          var viewsDomObj = " \
                            <div class='session-view__wrapper' id='id-"+id+"' data-track='"+trackId+"'> \
                            <h2 class='session-view__title'>"+title+"</h2> \
                            <h5 class='session-view__speaker'>"+speaker+speakerTitle+"</h5><br/> \
                            <div class='session-view__description'>"+desc+"</div> \
                            ";
          if (bio) viewsDomObj += " \
                                      <h3 class='session-view__bio-title'>About The Speaker</h3> \
                                      <p class='session-view__bio-speaker'><strong>"+speaker+"</strong>"+speakerTitle+"</p> \
                                      <p class='session-view__bio-text'>"+bio+"</p>\
                                      "
          viewsDom.push(
            // $('<div class="session-view__wrapper" id="id-'+id+' data-track="'+trackId+'""><h2 class="session-view__title">'+title+'</h2><h5 class="session-view__speaker">'+speaker+speakerTitle+'</h5><br/><div class="session-view__description">'+desc+'</div>')
            $(viewsDomObj)
          )
        }
      })

      // Append created objects to dom
      $('.session-aside').append(asideDom);
      $('.session-view').append(viewsDom);

      // update nav to refelect current track
      $('.session-nav__item').removeClass('active')
      $('.session-nav__item[data-track='+activeTrack+']').addClass('active');

      // update aside to show active article
      $('.session-aside__item').removeClass('active')
      $('.session-aside__item[data-article='+activeArticle+']').addClass('active');

    } // end setView

    // Query string parsing
    function getQueryVariable(variable) {
      var query = window.location.search.substring(1);
      var vars = query.split('&');
      for (var i = 0; i < vars.length; i++) {
          var pair = vars[i].split('=');
          if (decodeURIComponent(pair[0]) == variable) {
              return decodeURIComponent(pair[1]);
          }
          else { return false }
      }
    }

  })
})(jQuery);
